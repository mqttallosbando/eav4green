import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import it from 'vuetify/lib/locale/it';
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'fa',
    values: {
        user: 'fa-user-secret',
        logout: 'fa-sign-out-alt',
        home: 'fa-home',
        dashboard: 'fa-columns',
        device: 'mdi-devices',
        group:'fa-layer-group',
        network: 'fa-network-wired',
        ipnetwork: 'mdi-ip-network',
        subnet: 'mdi-check-network',
        gateway: 'fa-globe',
        dns: 'fa-random',
        yes: 'fa-check',
        no: 'fa-times',
        sendCommand: 'fa-cloud-upload-alt',
        waitCommand: 'fa-cloud-download-alt',
        deviceType1: 'fa-person-booth',
        deviceType2: 'fa-lightbulb',
        deviceType3: 'mdi-leak',
        deviceType4: 'mdi-camera-wireless',
        deviceType5: 'fa-plug',
        deviceType6: 'mdi-remote',
        deviceType8: 'fa-thermometer-quarter',
        deviceType9: 'fa-bullseye',
        password: 'fa-key',
        show: 'mdi-eye',
        hide: 'mdi-eye-off'
    },
  },   
  theme: {
      options: {
        customProperties: true,
      },
    themes: {
      light: {
        primary: '#1565C0',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107'
      },
    },   
  },
    lang: {
      locales: { it },
      current: 'it',
    },
});
