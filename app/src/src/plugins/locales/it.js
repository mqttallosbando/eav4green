export default { 
    "$app": {
      "close": "Chiudi",
      "loadingText": "Caricamento in corso...",
      "itemsPerPageText": "Righe per pagina:",
      "sortBy": "Ordina per",
      "noDataText": "Nessun elemento disponibile",
  
      "menu": {
        "dashboard": "Dashboard",
        "logout": "Esci"
      },

      "login":{
        "title": "Login",
        "subtitle": ""
      },
    }
}