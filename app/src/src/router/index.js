import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const ifAuthenticated = (to, from, next) =>
{
    let token = localStorage.getItem('user-token') || ''
    if (token != '')
    {
        next();
        return;
    }
    next('/login');
}

const routes = [
  {
    path: '/home',
    name: 'home',
    component: () => import('../views/Home.vue'),
    beforeEnter: ifAuthenticated,
  }, 
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/Login.vue'),
  },
/*   {
    path: '/group',
    name: 'group',
    component: () => import('../views/Group.vue'),
    beforeEnter: ifAuthenticated
  },
  {
    path: '/group/:group',
    name: 'groupDetail',
    component: () => import('../views/GroupDetail.vue'),
    beforeEnter: ifAuthenticated,
    props: true
  },  */   
  {
    path: '/*',
    name: 'redirect',
    component: () => import('../views/Home.vue'),
    beforeEnter: ifAuthenticated
  }
]

export default new VueRouter({
  routes,
  mode: 'history'
})