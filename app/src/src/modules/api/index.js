import axios from 'axios'

const httpSSl = require('https');

const myAxiosInstance = axios.create({
    baseURL: 'http://127.0.0.1:8000',
    timeout: 0,
    cache: false,
    headers: {
        'Content-Type': 'application/json'
    },
    httpsAgent: new httpSSl.Agent({  
       rejectUnauthorized: false
     })
})

export default {
    myAxiosInstance
}