//AUTH
export const AuthReq = 'AuthReq';
export const AuthRequestSucc = 'AuthRequestSucc';

export const AuthErr = 'AuthErr';
export const AuthSignOff = 'AuthSignOff';


//USER
export const UserReq = 'UserReq';
export const UserReqSucc = 'UserReqSucc';

export const UserErr = 'UserErr';

//COMMON
export const SetSnackbar = 'SetSnackbar';