import {
    AuthReq, AuthRequestSucc,
    AuthSignOff,
    AuthErr,
    
    UserReq
    
    } from '@/store/actions.js';
    
    import api from '@/modules/api/index';
    
    const state = {
        status: '',
        token: localStorage.getItem('user-token') || '',
        profile: {},
        site: {},
        //permissions: [],
    }
    
    const getters = {
        isLoggedin: state => !!state.token,
        authStatus: state => state.status == 'loading',
        //permissions: state => state.permissions,    
    }
    
    const actions =
    {
        [AuthReq]: ({ commit, dispatch }, credentials) => {
            return new Promise((resolve, reject) => {
    
                commit(AuthReq);
    
                //const token = localStorage.getItem('user-token') || 'Basic ' + window.btoa(credentials.username + ':' + credentials.password); // encode a string
                api.myAxiosInstance
                    .post('/api-auth/login/', credentials)
                    .then(resp => {
                        localStorage.setItem('user-token', "Token "+resp.data.key);
                        api.myAxiosInstance
                            .defaults.headers.common['Authorization'] = "Token "+resp.data.key;
                        commit(AuthRequestSucc, "Token "+resp.data.key);
                        dispatch(UserReq)
                        resolve();
                    })
                    .catch(err => {
                        commit(AuthErr, err.response);
                        localStorage.removeItem('user-token');
                        reject(err.response);
                    });
            });
        },
    
        [AuthSignOff]: ({ commit, dispatch }) => {
    
            return new Promise((resolve) => {
                commit(AuthSignOff);
                localStorage.removeItem('user-token');
                resolve();
            });
        }
    }
    
    const mutations =
    {
        [AuthReq]: (state) => {
            state.status = 'loading';
        },
    
        [AuthRequestSucc]: (state, payload) => {
            state.status = 'success';
            state.token = payload;
            //Vue.set(state, 'profile', payload.user);        
        }, 
    
        [AuthErr]: (state) => {
            state.token = '';
            state.status = 'error';
        },
    
        [AuthSignOff]: (state) => {
            state.token = '';
        }
    }
    
    export default {
        state,
        getters,
        actions,
        mutations,
    }
    