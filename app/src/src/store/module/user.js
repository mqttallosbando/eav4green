import
{
    UserReq, UserReqSucc,
    UserErr, AuthSignOff

} from '@/store/actions.js';

import Vue from 'vue';
import api from '@/modules/api/index';

const state = {
    status: '',
    profile: {},

    errors: null
}

const getters = {
    profile: state => state.profile,
    isProfileLoaded: state => !!state.profile.username,
    userSite: state => state.profile
}

const actions =
{
    [UserReq]: ({ commit, dispatch}) =>
    {
        api.myAxiosInstance
            .get('/api/user/', 
                { headers: { 'Authorization': localStorage.getItem('user-token') || '', } })
            .then(resp =>
            {
                console.log(resp)
                commit(UserReqSucc, resp.data[0]);
            })
            .catch(err =>
            {
                commit(UserErr, err.response);
                dispatch(AuthSignOff);
            })
    },
}

const mutations =
{
    [UserReq]: (state) =>
    {
        state.status = 'loading';
        Vue.set(state, 'errors', null);
    },

    [UserReqSucc]: (state, payload) =>
    {
        state.status = 'success';
        state.profile = payload;
        Vue.set(state, 'profile', payload);
    },

    [UserErr]: (state, payload) =>
    {
        state.status = 'error';
        Vue.set(state, 'errors', payload);
    },

    [AuthSignOff]: (state) =>
    {
        state.profile = {};
    }
}

export default {
    state,
    getters,
    actions,
    mutations,
}
