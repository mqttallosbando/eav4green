import
{
    SetSnackbar

} from '@/store/actions.js';

import Vue from 'vue';
import api from '@/modules/api/index';

const state = {
    snackbar: {
        text:'',
        show:false
    }
}

const getters = {
    snackbar: state => state.snackbar
}

const actions =
{
    [SetSnackbar]: ({ commit }, snackbar) =>
    {
        commit(SetSnackbar, snackbar)
    },
}

const mutations =
{
    [SetSnackbar]: (state, snackbar) =>
    {
        state.snackbar = snackbar
    }
}

export default {
    state,
    getters,
    actions,
    mutations,
}
