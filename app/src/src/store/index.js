import Vue from 'vue'
import Vuex from 'vuex'
import auth from '../store/module/auth';
import user from '../store/module/user';
import common from '../store/module/common';
Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    user,
    common
  }
})
