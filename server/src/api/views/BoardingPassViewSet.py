from django.contrib.auth.models import User
from django.http import HttpResponse
from rest_framework import status, viewsets, mixins
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions
from rest_framework.decorators import action
import os
from api.serializers.BoardingPassSerializer import BoardingPassSerializer
from api.models import BoardingPass


class BoardingPassViewSet(viewsets.ModelViewSet):
    queryset = BoardingPass.objects.none()
    serializer_class = BoardingPassSerializer
    #permission_classes = (IsAuthenticated, IsAdminOrUserDevice)
    lookup_field = 'serial_number__iexact'

    '''
    def get_serializer_class(self):
        if len(self.kwargs) == 0:
            if self.action == 'create':
                return DeviceSerializer
            if self.action == 'partial_update':
                return DeviceSerializer
        return UserDeviceSerializer
    '''
    def get_queryset(self):

        filter_args = {}
        user = self.request.user

        if user.is_superuser:
            return BoardingPass.objects.filter(**filter_args)
        else:
            return BoardingPass.objects.filter(userId=user, **filter_args)

    def destroy(self, request, pk=None):
        instance = self.get_object()
        # instance.isActive = False
        instance.isRecordValid = False
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

