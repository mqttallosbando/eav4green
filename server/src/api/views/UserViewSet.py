from django.contrib.auth.models import User
from rest_framework import status, viewsets, mixins
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, DjangoModelPermissions
from api.serializers.UserSerializer import UserSerializer


def get_valid_users(group=None):
    if group is not None:
        return User.objects.filter(is_active=True,
                                   anag__is_record_valid=True,
                                   anag__is_hidden=False,
                                   groups__name=group)
    return User.objects.filter(is_active=True,
                               anag__is_record_valid=True,
                               anag__is_hidden=False)


class UserViewSet(mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin,
                  mixins.ListModelMixin,
                  viewsets.GenericViewSet):
    """
    Lista utenti, accetta GET/DELETE.
    Dettaglio utenti, accetta anche POST/PUT/PATCH.
    L'oggetto anag e obbligatorio.

    list:
    Restituisce tutti gli utenti abilitati al login (settati come validi).

    destroy:
    Non elimina l'utente dal db, lo setta come inattivo e non valido.
    """
    queryset = User.objects.none()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated, DjangoModelPermissions)

    def get_queryset(self):
        user = self.request.user
        # result = User.objects.none()
        # is_many = True

        # Parametro da url, filtraggio per tipo (studente, insegnante)
        group = self.request.query_params.get('group', None)

        # L'utente e un Titolare:
        if user.groups.filter(name="Titolare").exists() or user.groups.filter(name="Addetto").exists():
            if group is not None:
                return get_valid_users(group=group)
            return get_valid_users()

        # Un superutente vedra tutti gli utenti attivi
        elif user.is_superuser:
            return User.objects.filter(is_active=True)
        # L'utente puo essere uno studente o meno, vedra solo i propri dati.
        else:
            return User.objects.filter(pk=user.pk)

        # return result

    def destroy(self, request, pk=None):
        """
        Previene la cancellazione del record dal db, che manteniamo per avere uno storico.
        """
        instance = self.get_object()
        instance.is_active = False
        instance.anag.is_record_valid = False
        instance.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
