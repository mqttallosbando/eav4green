from django.test import TestCase
from django.core.urlresolvers import resolve
from rest_framework import status
from rest_framework.test import force_authenticate
from rest_framework.test import APIClient
from django.contrib.auth.models import User


################################################################################
##                       Test Visualizzazione Users
################################################################################

class UserViewTest(TestCase):
    fixtures = ['initialdata.json']

    def setUp(self):
        self.client = APIClient()


    def test_user_owner(self):
        """
        Titolare vede tutti gli utenti
        """
        self.client.login(username='Titolare1', password='test')
        response = self.client.get('/api/user/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 5)
        self.client.logout()


    def test_user_student(self):
        """
        Studente vede solo se stesso
        """
        self.client.login(username='Studente1', password='test')
        response = self.client.get('/api/user/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.client.logout()


    def test_user_owner_filter(self):
        """
        Filtro per parametro group
        """
        self.client.login(username='Titolare1', password='test')
        response = self.client.get('/api/user/?group=Studente')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

        response = self.client.get('/api/user/?group=Insegnante')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        self.client.logout()


    def test_cant_see_password(self):
        """
        La password è write only
        """
        self.client.login(username='admin', password='admin')
        response = self.client.get('/api/user/')
        self.assertIsInstance(response.data, list)
        with self.assertRaises(KeyError):
            response.data[0]['password']
        self.client.logout()


################################################################################
##                       Test Creazione Users
################################################################################

class UserCreateTest(TestCase):
    fixtures = ['initialdata.json']

    def setUp(self):
        self.client = APIClient()


    def test_admin_cant_create_user(self):
        """
        La creazione dell'utente non può essere fatta. Bisogna passare dalla registrazione.
        """
        self.client.login(username='admin', password='admin')
        mock_data = self.client.get('/api/user/1/').data
        mock_data['password'] = 'asdasd'
        mock_data['username'] = 'asdasd'
        response = self.client.put('/api/user/', mock_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.client.logout()



################################################################################
##                       Test Cancellazione Users
################################################################################

class UserDeleteTest(TestCase):
    fixtures = ['initialdata.json']

    def setUp(self):
        self.client = APIClient()


    def test_admin_can_delete_user(self):
        """
        Test cancellazione utente da API, utente superuser
        """

        self.client.login(username='admin', password='admin')

        users_count_pre = len(self.client.get('/api/user/').data)
        response = self.client.delete('/api/user/2/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        users_count_post = len(self.client.get('/api/user/').data)

        self.assertEqual(users_count_post, users_count_pre-1)
        self.client.logout()

################################################################################
##                       Test Modifica Users
################################################################################

class UserEditTest(TestCase):
    fixtures = ['initialdata.json']

    def setUp(self):
        self.client = APIClient()



    def test_owner_can_edit_user(self):
        """
        Test modifica da parte dell'admin
        """
        self.client.login(username='admin', password='admin')

        user_pre = self.client.get('/api/user/1/').data
        user_pre['anag']['name'] = 'administrator'
        response = self.client.patch('/api/user/1/', user_pre, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        user_post = self.client.get('/api/user/1/').data
        self.assertEqual(user_post['anag']['name'], 'administrator')

        self.client.logout()


    def test_owner_can_edit_user_field(self):
        """
        Test modifica da parte dell'admin, con una sola voce
        """

        self.client.login(username='admin', password='admin')

        response = self.client.patch('/api/user/1/',
                                     {'anag': {'name': 'administrator'}},
                                     format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        user_post = self.client.get('/api/user/1/').data
        self.assertEqual(user_post['anag']['name'], 'administrator')

        self.client.logout()


    def test_owner_cant_edit_emailaddress(self):
        """
        Test modifica email, non deve essere possibile
        """
        self.client.login(username='admin', password='admin')

        user_pre = self.client.get('/api/user/1/').data
        user_pre['email'] = 'asdasd@localhost'
        response = self.client.patch('/api/user/1/', user_pre, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        user_post = self.client.get('/api/user/1/').data
        self.assertNotEqual(user_post['email'], 'asdasd@localhost')

        self.client.logout()



    def test_edit_non_existant_field(self):
        """
        Test modifica da parte dell'admin, con un campo inesistente
        """

        self.client.login(username='admin', password='admin')

        response = self.client.patch('/api/user/1/',
                                     {'anag': {'inesistente': 'test'}},
                                     format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.client.logout()


    def test_edit_wrong_field_type(self):
        """
        Test modifica da parte dell'admin, il valore inputato non è valido
        """

        self.client.login(username='admin', password='admin')

        response = self.client.patch('/api/user/1/',
                                     {'anag': {'cap': 'test'}},
                                     format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.client.logout()

