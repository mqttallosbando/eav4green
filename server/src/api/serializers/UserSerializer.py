from rest_framework import serializers
from rest_framework.utils import model_meta
from django.contrib.auth.models import User
from api.models import UserAnag


class UserAnagSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAnag
        fields = ('name',
                  'surname',
                  'subscribe_date',
                  'address',
                  'city',
                  'province',
                  'cap',
                  'phone',
                  'mobile_phone',
                  'last_updated')


class UserSerializer(serializers.ModelSerializer):
    anag = UserAnagSerializer(many=False)
    partial = True

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'anag')
        read_only_fields = ('email',)

    # Override metodo update(). Quello di ModelSerializer non supporta le relazione one-to-one
    # Nota: l'aggiornamento di un campo non esistente non ha alcun effetto, ne ritorna un errore.
    def update(self, instance, validated_data):
        if instance is None:
            return None

        info = model_meta.get_field_info(instance)

        # Preso dal codice di drf, ma e stata aggiunta la condizione per le relazioni one-to-one, facendo in
        # modo che queste vengano aggiornate in ricorsione
        for attr, value in validated_data.items():

            # Relazione many-to-many
            if attr in info.relations and info.relations[attr].to_many:
                field = getattr(instance, attr)
                field.set(value)

            # Relazione one-to-one
            elif attr in info.relations:
                # Aggiorno l'istanza 'figlia' riutilizzando lo stesso metodo.
                self.update(getattr(instance, attr), validated_data[attr])

            # Un campo semplice
            else:
                setattr(instance, attr, value)

        instance.save()
        return instance
