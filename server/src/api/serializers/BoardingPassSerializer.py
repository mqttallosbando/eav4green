from rest_framework import serializers
from rest_framework.utils import model_meta
from django.contrib.auth.models import User
from api.models import BoardingPass


class BoardingPassSerializer(serializers.ModelSerializer):

    class Meta:
        model = BoardingPass
        fields = ('serial_number', 'boarding_type','userId', 'redeemed_on', 'created_at')
        read_only_fields = ('created_at',)

    def create(self, data):
        request = self.context.get("request")
        if data['userId'] is None:
            data['userId'] = request.user
        bp = BoardingPass.objects.create(**data)        
        return bp