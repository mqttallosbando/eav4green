from allauth.account.adapter import DefaultAccountAdapter
from api.models import UserAnag


class AccountAdapter(DefaultAccountAdapter):

    def save_user(self, request, user, form, commit=True):
        user = super().save_user(request, user, form)
        if user:
            anag = UserAnag(user=user)
            anag.save()
        return user
