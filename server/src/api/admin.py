from django.contrib import admin
from api.models import *

from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

# Register your models here.

class UserAnagInline(admin.StackedInline):
    model = UserAnag
    can_delete = False
    verbose_name_plural = 'UserAnags'

class UserAdmin(BaseUserAdmin):
    inlines = (UserAnagInline, )

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(BoardingPass)

