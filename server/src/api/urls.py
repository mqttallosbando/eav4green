from django.conf.urls import url, include
from rest_framework import routers
from api.views.UserViewSet import UserViewSet
from api.views.BoardingPassViewSet import BoardingPassViewSet

router = routers.SimpleRouter()
router.register(r'user', UserViewSet, basename='user')
router.register(r'boardingpass', BoardingPassViewSet, basename='boardingpass')
urlpatterns = [
    url(r'^', include(router.urls)),
]
