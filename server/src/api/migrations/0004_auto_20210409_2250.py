# Generated by Django 3.1.3 on 2021-04-09 22:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20210409_2243'),
    ]

    operations = [
        migrations.AddField(
            model_name='boardingpass',
            name='id',
            field=models.AutoField(auto_created=True, default=0, primary_key=True, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='boardingpass',
            name='serial_number',
            field=models.CharField(max_length=15),
        ),
    ]
