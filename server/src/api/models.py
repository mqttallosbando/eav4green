from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class UserAnag(models.Model):
    user = models.OneToOneField(User, related_name='anag', on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    subscribe_date = models.DateField(auto_now=True)

    address = models.CharField(max_length=127, null=True, blank=True)
    city = models.CharField(max_length=50, null=True, blank=True)
    province = models.CharField(max_length=2, null=True, blank=True)
    cap = models.PositiveIntegerField(null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    mobile_phone = models.CharField(max_length=20, null=True, blank=True)
    last_updated = models.DateTimeField(null=True, blank=True)

    # Un valore "False" vuol dire che il record non è valido. Potrebbe essere stato
    # cancellato oppure annullato!
    is_record_valid = models.BooleanField(blank=False, null=False, default=True)
    is_hidden = models.BooleanField(blank=False, null=False, default=False)

    def __str__(self):
        return self.name + " " + self.surname


class BoardingPass(models.Model):
    serial_number = models.CharField(primary_key=True, max_length=15)
    BOARDINGPASS_CHOICES = [
        (0,"EAV"),
        (1,"UNICO")
    ]
    boarding_type = models.CharField(max_length=9,choices=BOARDINGPASS_CHOICES)
    userId = models.ForeignKey(User, null=True, related_name='userId', on_delete=models.CASCADE)
    redeemed_on = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now=True)